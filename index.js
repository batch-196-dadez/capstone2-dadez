const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;
mongoose.connect("mongodb+srv://joshuadad:admin1234@dadezzuitt.5gynprp.mongodb.net/Capstone-2-API?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection;

db.on('error',console.error.bind(console, "MongoDB Connection Error."));
db.once('open',()=>console.log("Successfuly Connected to MongoDB."))

app.use(express.json());

//
const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

//
const productRoutes = require('./routes/productRoutes');
app.use('/products',productRoutes);

//
const orderRoutes = require('./routes/orderRoutes');
app.use('/orders',orderRoutes);

app.listen(port,()=>console.log("Running at localhost Port:4000"))