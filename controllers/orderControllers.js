const Order = require("../models/Order");
const Product = require("../models/Product");


module.exports.viewAll = (req,res)=>{
	Order.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
};
module.exports.viewOrders = (req,res)=>{
	Order.find({userId:req.user.id},)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.viewProductsPerOrder = (req,res)=>{

	Order.findOne({userId:req.user.id})
	.then(result => {
		if(result === null){
			res.send("Please try again")
		} else {
			res.send(result)
		}

	}).catch(error => res.send(error))
};

module.exports.createOrder = (req,res)=>{
	if(req.user.isAdmin=false){
		return res.send({message: "Denied."});
	} 

		let newOrder = new Order({
			totalAmount: req.body.totalAmount,
			userId: req.user.id,
			products: req.body
		})
	
		newOrder.save().
		then(result => res.send({message: "Order Successful"}))
		.catch(error => res.send(error))

	}


