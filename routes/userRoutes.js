const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const { verify } = auth;

const { verifyAdmin } = auth;



//user routes

router.get("/",verify, verifyAdmin, userControllers.viewAll);
router.post("/register",userControllers.register);
router.post("/login", userControllers.login);
router.get("/getUserDetails", verify, userControllers.viewUser);
router.put("/setAdmin/:userId", verify, verifyAdmin, userControllers.setAdmin);


module.exports = router;