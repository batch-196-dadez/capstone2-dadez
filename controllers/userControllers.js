const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.viewAll = (req,res)=>{
    User.find({})
    .then(result => res.send(result)).
    catch(error => res.send(error))
}


module.exports.register = (req,res) => {
	User.find({email:req.body.email})
	.then(result => {
		if(result.length === 0){
			const hashedPw = bcrypt.hashSync(req.body.password,10)
    
			let newUser = new User({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email,
				password: hashedPw,
				mobileNo: req.body.mobileNo
		
			})
			newUser.save().then(result => res.send(result)).catch(error => res.send("Error("))
		} else {
			res.send("Email already in use.")
		}
	}
	).catch(error => res.send(error))

}

module.exports.login = (req,res) => {
	User.findOne({email:req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			res.send({message: "No User Found."})

		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			if(isPasswordCorrect){
				return res.send(foundUser.firstName + auth.createAccessToken(foundUser))

			} else {
				return res.send({message: "Incorrect Password"})
			}

		}

	}).catch(() => "Error:(")
}

module.exports.viewUser = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


// make a user admin
module.exports.setAdmin = (req,res) => {
	let update = {
		isAdmin: true
	}
	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(" Try again."))
}
module.exports.removeAdmin = (req,res) => {
	let update = {
		isAdmin: false
	}
	User.findByIdAndUpdate(req.params.userId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
