
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount: {type: Number,required: [true,"Please fill up"]},
    purchasedOn: {type: Date,default: new Date()},
    userId: {
        type: String,
        required: [true, "Please fill up"]
    },
	products: [

        {
            productId: {type: String, required: [true," Please fill up"]},
            quantity: {type: Number}
        }

    ]	
});

	module.exports = mongoose.model("Order",orderSchema);