
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {type:String,required: [true,"Please fill up"]},
	lastName: {type: String,required: [true,"Please fill up"]},
	email: {type: String,required: [true,"Please fill up"]},
	password: {type: String,required: [true,"Please fill up"]},
    mobileNo: {type: String,required: [true,"Please fill up"]},
	isAdmin: {type: Boolean,default: false}
	})

	module.exports = mongoose.model("User",userSchema);