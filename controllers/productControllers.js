const Product = require("../models/Product");
const auth = require("../auth");
const Order = require("../models/Order");
// retrieve all products

module.exports.getAllProducts = (req,res)=>{

	Product.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};
//sort
module.exports.sortByOrder = (req,res)=>{
	sort = {numberOfOrders: -1}
	Product.find({isActive:true}).sort(sort)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


// retrieve a single product
module.exports.getSingleProduct = (req,res)=>{
	Product.findOne({id:req.params.productId})
	.then(result => {
        if(result){res.send(result)}
        else {res.send("Not Available")}})
	.catch(error => res.send(error))
};

module.exports.createProduct = (req,res)=>{
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newProduct.save().then(result => res.send("Successfully" + result)).catch(error => res.send("Invalid input!"))
}

module.exports.updateProduct = (req,res)=>{
	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
        updatedOn: new Date()
	}

	Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => {res.send("Successfully updated" + result)})
	.catch(error => res.send(error))
}

module.exports.archiveProduct = (req,res)=>{
    let update = {isActive: false}
    Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => {res.send("Product Archived " + result)})
	.catch(error => res.send(error))
}

module.exports.activateProduct = (req,res)=>{
    let update = {
        isActive: true
    }
    Product.findByIdAndUpdate(req.params.productId,update,{new:true})
	.then(result => {res.send("Product successfully activated" + result)})
	.catch(error => res.send(error))
}