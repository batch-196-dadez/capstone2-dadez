const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");

const { verify } = auth;

const { verifyAdmin } = auth;

router.get("/", productControllers.getAllProducts);

router.get("/sortByOrder", productControllers.sortByOrder);

router.get("/:productId", productControllers.getSingleProduct);

router.post("/createProduct", verify, verifyAdmin, productControllers.createProduct);

router.put("/updateProduct/:productId", verify, verifyAdmin, productControllers.updateProduct);

router.put("/archiveProduct/:productId", verify, verifyAdmin, productControllers.archiveProduct);

router.put("/activateProduct/:productId", verify, verifyAdmin, productControllers.activateProduct);

module.exports = router;